import React from 'react';
import LOGO from "../../assets/images/pokemonLogo.png"
import { Link } from 'react-router-dom';

const HomePage = () => (
    <div className="home">
        <div className="home-content">
            <img src={LOGO} alt=""/>
            <button className="btn">
                <Link to="/map">START</Link>
            </button>
        </div>
    </div>
);

export default HomePage;
