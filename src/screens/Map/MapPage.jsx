import React from 'react';
import Sidebar from '../../components/Sidebar';

const MapPage = () => {

    return (
        <>
            <div className="map">
                <Sidebar />
            </div>
        </>
    );
};

export default MapPage;
