import { object } from "prop-types";

const TYPES = {
  CAPTURE: "CAPTURE",
};

const INITIAL_STATE = [];

export const captureReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.CAPTURE:
      if (typeof action.payload === "number") {
        let d = state.filter((el) => el.id !== action.payload);
        return [...d];
      }
      return [...state, action.payload];
    default:
      return state;
  }
};

export const captureAction = (data) => {
  let obj = {};
  if (typeof data === object) {
    obj = { ...data, captured: true };
  } else {
    obj = data;
  }

  return { type: TYPES.CAPTURE, payload: obj };
};
