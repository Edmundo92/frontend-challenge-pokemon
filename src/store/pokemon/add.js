const { api } = require("../../services/api");

const TYPES = {
    ADD: "ADD"
}

const INITIAL_STATE = [];

export const captureReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPES.ADD: return [...state, {...action.payload}];
        default: return state;
    }
};

export const captureAction = (data) => {
    return (dispatch) => {
        api
          .post(`/pokemon`, data)
          .then((res) => {
            dispatch({ type: TYPES.ADD, payload: res.data });
          })
          .catch((err) => {});
      };
}