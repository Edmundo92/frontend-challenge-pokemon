const { api } = require("../../services/api");

const TYPES = {
  FIND: "FIND",
  FIND_ERROR: "FIND_ERROR",
};

const INITIAL_STATE = {};

export const findReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.FIND:
      return { ...state, ...action.payload };
    case TYPES.FIND_ERROR:
      return { ...action.payload };
    default:
      return state;
  }
};

export const findAction = (id, captured = false) => {
  return (dispatch) => {
    api
      .get(`/pokemon/${id}`)
      .then((res) => {
        if (captured) {
          dispatch({
            type: TYPES.FIND,
            payload: { ...res.data, captured: true, error: false },
          });
        } else {
          dispatch({
            type: TYPES.FIND,
            payload: { ...res.data, captured: false, error: false },
          });
        }
      })
      .catch((err) => {
        dispatch({
          type: TYPES.FIND_ERROR,
          payload: {},
        });
      });
  };
};
