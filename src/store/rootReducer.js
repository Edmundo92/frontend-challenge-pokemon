import { combineReducers } from "redux";

// reducers
import { findReducer } from "./pokemon/find";
import { captureReducer } from "./pokemon/capture";

const rootReducer = combineReducers({
  find: findReducer,
  capture: captureReducer,
});

export default rootReducer;
