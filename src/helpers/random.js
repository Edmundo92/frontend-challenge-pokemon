const startNumber = 1;
const toNumber = 1000;

export default function() {
    return Math.floor(Math.random() * toNumber) + startNumber;
}