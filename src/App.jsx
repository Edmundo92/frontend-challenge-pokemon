import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import MapPage from './screens/Map/MapPage';
import HomePage from './screens/Home/HomePage';

const App = () => (
    <Switch>
        <Route path="/home" component={HomePage} />
        <Route path="/map" component={MapPage} />
        <Redirect to="/map" />
    </Switch>
);

export default App;
