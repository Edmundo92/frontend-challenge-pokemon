import React from "react"

const Block = ({ children, label }) => {
    return (
        <div className="block">
            <div className="block-content-title">
                <hr/>
                <h4 className="block-title">{  label }</h4>
                <hr/>
            </div>
            <div className="block-content">
                { children }
            </div>
        </div>
    )
}

export default Block;