import React from "react"

const AshAnimation = () => {
    
    return (
        <div className="ash-content-animation">
            <img 
                className="ashRightLeg"
                src={require("../assets/images/ashRightLeg.png")} 
                alt=""
            />
            <img 
                className="ashLeftLeg"
                src={require("../assets/images/ashLeftLeg.png")} 
                alt=""
            />
        </div>
    )
}

export default AshAnimation;