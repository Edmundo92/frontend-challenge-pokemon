import React, { useState } from "react";

const Select = ({ name }) => {
  const [classOptions, setClassOptions] = useState();
  const [isOpen, setIsOpen] = useState(false);
  const [value, setValue] = useState("");

  const data = ["Hummer", "Ranger Rover"];

  function openOptions() {
    setIsOpen(!isOpen);
  }

  function handleChangeValue(value) {
    setValue(value);
    openOptions();
  }

  return (
    <div className="select-container">
      <div className="custom-select">
        <img
          src={require("../assets/images/chevronBottom.svg")}
          className="arrow-down-icon"
          onClick={openOptions}
        />
        <input type="hidden" name={name} />
        <div
          className={
            isOpen ? "select-selected select-arrow-active" : "select-selected"
          }
          onClick={openOptions}
        >
          {value}
        </div>
        <div className={isOpen ? "select-items" : "select-items select-hide"}>
          {data &&
            data.map((el) => (
              <div
                key={el}
                onClick={() => handleChangeValue(el)}
                style={
                  el === value ? { backgroundColor: "rgba(0,0,0,.1)" } : {}
                }
              >
                {el}
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

export default Select;
