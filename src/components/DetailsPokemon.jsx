import React, { useEffect, useState } from "react";
import Input from "./Input";
import Avatar from "./Avatar";
import Block from "./Block";

import { captureAction } from "../store/pokemon/capture";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

const DetailsPokemon = ({ data, captureAction, onCancel }) => {
  const {
    id,
    name,
    weight,
    height,
    types,
    abilities,
    sprites,
    stats,
    captured,
  } = data;
  const [namePokemon, setNamePokemon] = useState("");
  const [isShowFieldEdit, setIsShowFieldEdit] = useState(false);

  const palleteColor = ["#2E3A59", "#924990", "#00D68F", "#DB2C66"];

  useEffect(() => {
    setNamePokemon(name);
  }, [data]);

  function capture() {
    captureAction(data);
  }

  function releasePokemon() {
    captureAction(id);
  }

  function handleChangeName({ value }) {
    setNamePokemon(value);
  }

  function saveNameChanged() {
    data["name"] = namePokemon;
    setIsShowFieldEdit(false);
  }

  return (
    <div className="avatar-container">
      <div className="avatar-content">
        <img
          className="close"
          onClick={onCancel}
          src={require("../assets/images/close.svg")}
        />
        <Avatar
          url={
            sprites.front_default
              ? sprites.front_default
              : require("../assets/images/editIcon.png")
          }
        />
        <div className="avatar-content-bottom">
          <div className="content-name">
            {!isShowFieldEdit && <span className="title"> {namePokemon} </span>}
            {captured && (
              <img
                className="icon-edit"
                onClick={() => setIsShowFieldEdit(true)}
                src={require("../assets/images/editIcon.png")}
                alt=""
              />
            )}
          </div>

          {isShowFieldEdit && (
            <div className="content-form">
              <Input
                type="text"
                value={namePokemon}
                handleEvent={handleChangeName}
              />
              <div className="content-btn" onClick={saveNameChanged}>
                <button className="btn-action">
                  <img src={require("../assets/images/checkIcon.png")} alt="" />
                </button>
                <button
                  className="btn-action"
                  onClick={() => setIsShowFieldEdit(false)}
                >
                  <img src={require("../assets/images/close.svg")} alt="" />
                </button>
              </div>
            </div>
          )}
          <div className="details-content">
            <ul className="detail-list">
              <li key="detail-item-1" className="detail-item">
                <h6> HP </h6> <span> {stats[0].base_stat} </span>
              </li>
              <li key="detail-item-2" className="detail-item">
                <h6> ALTURA </h6> <span> {height}m </span>
              </li>
              <li key="detail-item-3" className="detail-item">
                <h6> PESO </h6>
                <span>
                  {weight}
                  kg
                </span>
              </li>
            </ul>
          </div>

          <Block label="TIPO">
            <div className="content-type">
              <div className="content-type-button">
                {types.map(({ type }, index) => {
                  return (
                    <button
                      key={type.name}
                      className="btn-type"
                      style={{ backgroundColor: palleteColor[index] }}
                    >
                      {type.name}
                    </button>
                  );
                })}
              </div>
            </div>
          </Block>

          <Block label="HABILIDADES">
            <div className="content-habilities">
              <div className="habilities">
                <p>
                  {abilities.map((el) => {
                    return (
                      <span key={el.ability.name}> {el.ability.name}, </span>
                    );
                  })}
                </p>
              </div>
            </div>
          </Block>

          {captured && (
            <Block label="ESTATÍSTICAS">
              <div className="content-statistics">
                <ul className="statistics-list">
                  <li key="statistics-1" className="">
                    <div className="">
                      <img
                        src={require("../assets/images/shield.svg")}
                        alt=""
                      />
                      <span> DEFESA </span>
                    </div>
                    <span> {stats[2].base_stat} </span>
                  </li>
                  <li key="statistics-2" className="">
                    <div className="">
                      <img src={require("../assets/images/sword.svg")} alt="" />
                      <span> ATAQUE </span>
                    </div>
                    <span> {stats[1].base_stat} </span>
                  </li>
                  <li key="statistics-3" className="">
                    <div className="">
                      <img
                        src={require("../assets/images/shield.svg")}
                        alt=""
                      />
                      <span> DEFESA ESPECIAL </span>
                    </div>
                    <span> {stats[4].base_stat} </span>
                  </li>
                  <li key="statistics-4" className="">
                    <div className="">
                      <img src={require("../assets/images/sword.svg")} alt="" />
                      <span> ATAQUE ESPECIAL </span>
                    </div>
                    <span> {stats[3].base_stat} </span>
                  </li>
                  <li key="statistics-5" className="">
                    <div className="">
                      <img src={require("../assets/images/timer.svg")} alt="" />
                      <span> VELOCIDADE </span>
                    </div>
                    <span> {stats[5].base_stat} </span>
                  </li>
                </ul>
              </div>
            </Block>
          )}

          {captured ? (
            <button className="btn-l" onClick={releasePokemon}>
              LIBERAR POKEMON
            </button>
          ) : (
            <img
              className="pokeball"
              src={require("../assets/images/pokeball.png")}
              onClick={capture}
            />
          )}
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      captureAction,
    },
    dispatch
  );

export default connect(null, mapDispatchToProps)(DetailsPokemon);
