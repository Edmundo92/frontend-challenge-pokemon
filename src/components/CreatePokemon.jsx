import React from "react";
import Input from "./Input";
import Avatar from "./Avatar";
import Block from "./Block";
import InputNumber from "./InputNumber";
import Select from "./Select";

const CreatePokemon = ({ onCancel }) => {
  return (
    <div className="avatar-container">
      <div className="avatar-content">
        <img
          className="close"
          onClick={onCancel}
          src={require("../assets/images/close.svg")}
        />
        <Avatar />
        <div className="avatar-content-bottom">
          <div className="content-name"></div>
          <div className="form-create">
            <Input type="text" label="NOME" />
            <InputNumber label="HP" />
            <InputNumber label="PESO" unity="KG" />
            <InputNumber label="ALTURA" unity="Cm" />
          </div>

          <Block label="TIPO">
            <Select />
          </Block>
          <Block label="HABILIDADES">
            <Input type="text" placeholder="Habilidade 1" />
            <Input type="text" placeholder="Habilidade 2" />
            <Input type="text" placeholder="Habilidade 3" />
            <Input type="text" placeholder="Habilidade 4" />
          </Block>
          <Block label="ESTATÍSTICAS">
            <InputNumber
              label="DEFESA"
              icon={require("../assets/images/shield.svg")}
            />
            <InputNumber
              label="ATAQUE"
              icon={require("../assets/images/sword.svg")}
            />
            <InputNumber
              label="DEFESA ESPECIAL"
              icon={require("../assets/images/shield.svg")}
            />
            <InputNumber
              label="ATAQUE ESPECIAL"
              icon={require("../assets/images/sword.svg")}
            />
            <InputNumber
              label="VELOCIDADE"
              icon={require("../assets/images/timer.svg")}
            />
          </Block>

          <button className="btn-l">CRIAR POKEMON</button>
        </div>
      </div>
    </div>
  );
};

export default CreatePokemon;
