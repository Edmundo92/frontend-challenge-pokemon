import React from "react";

const Input = ({ type, label, placeholder, handleEvent, value }) => {
  return (
    <div className="input-form-content">
      {label && <label> {label} </label>}
      <input
        className="input-form"
        type={type}
        value={value}
        placeholder={placeholder}
        onChange={(e) => handleEvent(e.target)}
      />{" "}
    </div>
  );
};

export default Input;
