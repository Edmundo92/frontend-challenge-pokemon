import React from "react"

const Avatar = ({url}) => {
    return (
        <div className="avatar-content-top">
            <img className="img-avatar" src={url} alt=""/>
        </div>
    )
}

export default Avatar;