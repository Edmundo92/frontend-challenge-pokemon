import React, { useState } from "react";

const InputNumber = ({ label, unity, icon }) => {
    const [value, setValue] = useState(0);

    function decrement() {
        setValue(value - 1);
    }
    
    function increment() {
        setValue(value + 1);
    }

    function changeValue(e) {
        const value = e.target.value;
        if(value) {
            setValue(parseInt(value));
        }
    }

    return (
        <div className="input-number-content">
            { label && 
                <label className="custom-label">
                    <img src={icon} alt=""/>
                    { label }
                </label> 
            }
            <div className="input-number">
                <input 
                    type="number" 
                    value={value} 
                    className="input-form" 
                    onChange={changeValue}
                />                        
                <span> { unity } </span>
                <div>
                    <button onClick={increment}>
                        <img src={require("../assets/images/chevronTop.svg")} alt=""/>
                    </button>
                    <button onClick={decrement}>
                    <img src={require("../assets/images/chevronBottom.svg")} alt=""/>
                    </button>
                </div>
            </div>
        </div>
    )
}

export default InputNumber;