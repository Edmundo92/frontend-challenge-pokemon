import React from "react";
import PropTypes from "prop-types";
import { useTransition, animated } from "react-spring";

const Modal = ({ children, isShow, onCancel }) => {
  const props = useTransition(isShow, null, {
    from: { opacity: 0, marginTop: "-50px" },
    enter: { opacity: 1, marginTop: "0px" },
    leave: { opacity: 0, marginTop: "150px" },
  });

  const propsWrapper = useTransition(isShow, null, {
    from: { opacity: 0 },
    enter: { opacity: 0.4 },
    leave: { opacity: 0 },
  });

  // return (
  //     <div className="modal">
  //         <div className="modal__content">{children}</div>
  //     </div>
  // )

  return (
    <React.Fragment>
      {propsWrapper.map(({ item, styles, key }) => {
        return item ? (
          <animated.div key={key} style={styles} className="modal">
            {props.map(({ item, props, key }) => {
              return item ? (
                <animated.div
                  key={key}
                  style={props}
                  className="modal__content"
                >
                  {children}
                </animated.div>
              ) : null;
            })}
          </animated.div>
        ) : null;
      })}
    </React.Fragment>
  );
};

Modal.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Modal;
