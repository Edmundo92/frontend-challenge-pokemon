import React, { useState, useEffect } from "react";
import ASH from "../assets/images/ashFront.png";
import FIND from "../assets/images/findTooltip.png";
import Modal from "./Modal";
import DetailsPokemon from "./DetailsPokemon";
import CreatePokemon from "./CreatePokemon";
import AshAnimation from "./AshAnimation";

import { api } from "../services/api";
import random from "../helpers/random";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { findAction } from "../store/pokemon/find";
import { isEmpty } from "../helpers/empty";

const Sidebar = ({ findAction, data, pokemonsCapturedList }) => {
  const LIMIT_POKEMON_CAPTURED = 6;

  const [hover, setHover] = useState(false);
  const [captured, setCaptured] = useState([]);
  const [isLimit, setIsLimit] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [typeModal, setTypeModal] = useState();
  const [pokemonCaptured, setPokemonCaptured] = useState({});
  const [isShowModal, setIsShowModal] = React.useState(false);
  const [isError, setIsError] = React.useState(false);

  useEffect(() => {
    if (!isEmpty(data || pokemonCaptured)) {
      setIsLoading(false);
      setTimeout(() => {
        setIsShowModal(true);
        setHover(false);
      }, 500);
    } else {
      setIsError(true);
      setIsLoading(false);
      setTimeout(() => {
        setIsError(false);
        setHover(false);
      }, 1000);
    }
  }, [data, pokemonCaptured]);

  useEffect(() => {
    const pokemonsCapturedListLength = pokemonsCapturedList.length;
    if (pokemonsCapturedListLength <= LIMIT_POKEMON_CAPTURED) {
      setCaptured(pokemonsCapturedList);
      setIsShowModal(false);
    }

    if (pokemonsCapturedListLength == LIMIT_POKEMON_CAPTURED) {
      setIsLimit(true);
    }
  }, [pokemonsCapturedList]);

  function findPokemons() {
    const randomNumber = random();
    findAction(randomNumber);
  }

  function showPokemonsCapturedList(pokemon) {
    findAction(pokemon["id"], true);
  }

  return (
    <>
      <div className="sidebar">
        {captured &&
          captured.map((pokemon, index) => {
            return (
              <div
                key={`pokemon_${index}`}
                style={{
                  backgroundRepeat: "no-repeat",
                  backgroundSize: "contain",
                  backgroundImage: `url(${pokemon.sprites.front_default})`,
                }}
                onClick={() => {
                  setTypeModal("Details");
                  showPokemonsCapturedList(pokemon);
                }}
                className="sidebar__item"
              ></div>
            );
          })}
        <div
          onClick={() => {
            setTypeModal("Create");
            setIsShowModal(true);
          }}
          className="sidebar__item add"
        >
          +
        </div>
      </div>

      <div className="content-avatar">
        <div className="content-avatar-box">
          {!isLimit && hover && (
            <img
              className="icon-find"
              src={
                !isError ? FIND : require("../assets/images/errorTooltip.png")
              }
              alt=""
              onClick={() => {
                setIsLoading(true);
                setTypeModal("Details");
                findPokemons();
              }}
              style={hover ? { display: "block" } : { display: "none" }}
            />
          )}
          {isLoading ? (
            <AshAnimation />
          ) : (
            <img
              className="ash-avatar"
              src={ASH}
              alt=""
              onMouseOver={() => setHover(true)}
            />
          )}
        </div>
      </div>

      <Modal isShow={isShowModal}>
        {typeModal === "Details" && (
          <DetailsPokemon
            onCancel={() => setIsShowModal(false)}
            data={data || pokemonCaptured}
          />
        )}
        {typeModal === "Create" && (
          <CreatePokemon onCancel={() => setIsShowModal(false)} />
        )}
      </Modal>
    </>
  );
};

const mapStateToProps = ({ find, capture }) => {
  return {
    data: find,
    pokemonsCapturedList: capture,
  };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ findAction }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
